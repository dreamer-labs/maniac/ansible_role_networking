#!/bin/bash
cd /home/NXOS_VB
output="$(vagrant snapshot restore nxos_clean 2>&1)"
if [[ "${output}" =~ config.ssh.shell ]]; then
  exit 0;
else
  exit 1;
fi;
